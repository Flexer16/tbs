---
-- main.lua


package.path = package.path .. ";lib/?/init.lua;lib/?.lua;src/?.lua"


local lovetoys = require "lovetoys"
local Signal = require "hump.signal"

local Grid = require "utils.grid"


lovetoys.initialize({
    globals = true,
    debug = true
})


require "ecs.prepare_components"


local UserInputsystem = require "ecs.systems.user_input_system"
local ViewSystem = require "ecs.systems.view_system"
local LoadSceneSystem = require "ecs.systems.load_scene_system"

function love.load()
    engine = Engine()
    signal = Signal.new()
    grid = Grid(65, 17)

    engine:addSystem(UserInputsystem(), "update")

    engine:addSystem(LoadSceneSystem(), "update")

    engine:addSystem(ViewSystem(), "draw")

    signal:emit("load-scene")
end


function love.update(dt)
    engine:update(dt)
end


function love.draw()
    engine:draw()
end

---
-- entity_factory.lua


local class = require "middleclass"

local ViewFactory = require "data.factories.view_factory"


local EntityFactory = class("EntityFactory")

function EntityFactory:initialize(data)
    self.view_factory = ViewFactory()
end

function EntityFactory:get_terrain(tile, x, y)
    local Position, View, Terrain = Component.load({"Position", "View", "Terrain"})

    local terrain = Entity()
    terrain:initialize()

    terrain:add(Terrain())
    terrain:add(Position({x = x, y = y}))

    local view = View({tile = tile})
    view:set_draw_func(self.view_factory:get_func(view:get_tile()))
    terrain:add(view)

    return terrain
end

return EntityFactory

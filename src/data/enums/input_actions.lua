---
-- input_actions.lua


local InputActions = {
    move_up = "move_up",
    move_down = "move_down",
    move_left = "move_left",
    move_right = "move_right"
}

return InputActions

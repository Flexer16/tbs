---
-- cell.lua


local class = require "middleclass"


local Cell = class("Cell")

function Cell:initialize(data)
    self.terrain = nil
end

function Cell:get_terrain()
    return self.terrain
end

function Cell:set_terrain(terrain)
    self.terrain = terrain
end

return Cell

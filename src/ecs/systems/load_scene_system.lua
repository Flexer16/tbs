---
-- load_scene_system.lua


local class = require "middleclass"

local EntityFactory = require "data.factories.entity_factory"
local MapData = require "data.map.map_data"
local Cell = require "data.world.cell"


local LoadSceneSystem = class("LoadSceneSystem", System)

function LoadSceneSystem:initialize(data)
    System.initialize(self)

    self.entity_factory = EntityFactory()

    signal:register("load-scene", function () self:_load_map("res/maps/test_map.json") end)
end

function LoadSceneSystem:update(dt)
    -- body
end

function LoadSceneSystem:requires()
    return {}
end

function LoadSceneSystem:onAddEntity(entity)
    -- body
end

function LoadSceneSystem:onRemoveEntity(entity)
    -- body
end

function LoadSceneSystem:_load_map(file_path)
    local map_data = MapData({file_path = file_path})
    local map_str = map_data:get_map_string()

    local x, y = 1, 1

    for row in map_str:gmatch("[^\n]+") do
        x = 1

        for tile in row:gmatch(".") do
            grid:set(x, y, Cell())

            local terrain = self.entity_factory:get_terrain(tile, x, y)

            engine:addEntity(terrain)
            grid:get(x, y):set_terrain(terrain)

            x = x + 1
        end

        y = y + 1
    end
end

return LoadSceneSystem

---
-- user_input_system.lua


local class = require "middleclass"
local Input = require "input"

local Actions = require "data.enums.input_actions"


local UserInputSystem = class("UserInputSystem", System)

function UserInputSystem:initialize(data)
    System.initialize(self)

    self.input = Input()

    self.key_table = {
        {"up", Actions.move_up},
        {"down", Actions.move_down},
        {"left", Actions.move_left},
        {"right", Actions.move_right}
    }

    self:_set_key_table()
end

function UserInputSystem:update(dt)
    for _, val in ipairs(self.key_table) do
        if self.input:down(val[2], 0.15) then
            signal:emit("input-action", val[2])
        end
    end
end

function UserInputSystem:requires()
    return {}
end

function UserInputSystem:onAddEntity(entity)
    -- body
end

function UserInputSystem:onRemoveEntity(entity)
    -- body
end

function UserInputSystem:_set_key_table()
    for _, val in ipairs(self.key_table) do
        self.input:bind(val[1], val[2])
    end
end

return UserInputSystem

---
-- view_system.lua


local class = require "middleclass"
local Gamera = require "gamera"

local CoordinateConverter = require "utils.coordinate_converter"
local Actions = require "data.enums.input_actions"


local ViewSystem = class("ViewSystem", System)

function ViewSystem:initialize(data)
    System.initialize(self)

    self.tile_size = 64

    self.coord_converter = CoordinateConverter({cell_size = self.tile_size})

    self.camera = Gamera.new(0, 0, grid:get_widht() * self.tile_size, grid:get_height() * self.tile_size)
    self.camera:setWindow(0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    self.camera:setPosition(0, 0)

    signal:register("input-action", function (action) self:_move_camera(action) end)
end

function ViewSystem:draw()
    self.camera:draw(function (l, t, w, h)
        self:_draw_map()
    end)
end

function ViewSystem:requires()
    return {}
end

function ViewSystem:onAddEntity(entity)
    -- body
end

function ViewSystem:onRemoveEntity(entity)
    -- body
end

function ViewSystem:_draw_map()
    local views = engine:getEntitiesWithComponent("Terrain")

    for _, view in pairs(views) do
        local loc_x, loc_y = view:get("Position"):get()
        local glob_x = self.coord_converter:get_world_pos(loc_x)
        local glob_y = self.coord_converter:get_world_pos(loc_y)

        view:get("View"):draw_at(glob_x, glob_y)
    end
end

function ViewSystem:_move_camera(action)
    local new_x, new_y = self.camera:getPosition()

    if action == Actions.move_up then
        new_y = new_y - self.tile_size
    end

    if action == Actions.move_down then
        new_y = new_y + self.tile_size
    end

    if action == Actions.move_left then
        new_x = new_x - self.tile_size
    end

    if action == Actions.move_right then
        new_x = new_x + self.tile_size
    end

    self.camera:setPosition(new_x, new_y)
end

return ViewSystem

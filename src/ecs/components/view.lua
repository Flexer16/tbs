---
-- view.lua


local class = require "middleclass"


local View = class("View")

function View:initialize(data)
    if not data then
        data = {}
    end

    self.tile = data.tile or "."
    self.base_tile = self.tile
    self.draw_func = data.draw_func or nil
end

function View:draw_at(x, y)
    self.draw_func(self.tile, x, y)
end

function View:set_draw_func(draw_func)
    self.draw_func = draw_func
end

function View:get_tile()
    return self.tile
end

function View:set_tile(tile)
    self.tile = tile
end

function View:get_base_tile()
    return self.base_tile
end

return View

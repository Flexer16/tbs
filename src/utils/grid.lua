---
-- grid.lua


local class = require "middleclass"


local Grid = class("Grid")

function Grid:initialize(x, y, default)
    self.default = default or nil

    self.width, self.height = x, y
    self.map = {}

    for j = 1, self.height do
        for i = 1, self.width do
            self.map[(i - 1) * self.width + j] = self.default
        end
    end
end

function Grid:get_widht()
    return self.width
end

function Grid:get_height()
    return self.height
end

function Grid:get(x, y)
    return self.map[(x - 1) * self.width + y]
end

function Grid:set(x, y, value)
    self.map[(x - 1) * self.width + y] = value
end

function Grid:check(x, y)
    if 1 <= x and x <= self.width and 1 <= y and y <= self.height then
        return true
    else
        return false
    end
end

function Grid:iterate()
    local i, j = 0, 1

    return function ()
        if i < self.width then
            i = i + 1
        else
            if j < self.height  then
                j = j + 1
                i = 1
            else
                return nil
            end
        end

        return i, j, self.map[(i - 1) * self.width + j]
    end
end

function Grid:print()
    for i, j, val in self:iterate() do
        io.write(tostring(val) .. " ")

        if i == self.width then
            io.write("\n")
        end
    end
end

return Grid
